const path              = require('path');
const webpack           = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const pkg               = require('./package.json');

module.exports = {
  context: path.resolve(__dirname, './app/Resources/assets/src'),

  entry  : {
    app: './app.js',
  },

  output : {
    path         : path.resolve(__dirname, './web/build'),
    filename     : '[name].js',
    chunkFilename: '[name].bundle.js',
    publicPath   : 'http://localhost:8090/assets/',
  },

  devtool  : 'eval',

  devServer: {
    historyApiFallback: true,
    hot               : true,
    hotOnly           : true,
    stats             : 'errors-only',
    contentBase       : path.join(__dirname, 'web/'),
    host              : 'localhost',
    port              : 8090
  },

  module : {
    rules: [
      {
        test: /\.js$/,
        use : [{
          loader : 'babel-loader',
          options: {
            presets    : ['es2015'],
            retainLines: true,
          }
        }],
      },

      {
        test: /\.css$/,
        use : [
          'style-loader',
          'css-loader?sourceMap',
        ]
      },

      {
        test: /\.(sass|scss)$/,
        use : [
          'style-loader',
          'css-loader?sourceMap',
          'sass-loader?sourceMap',
        ]
      },

      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use : [
          'url-loader?limit=10000&mimetype=application/font-woff',
        ]
      },

      {
        test: /\.(png|gif|jpe?g|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use : [
          'file-loader',
        ]
      }
    ],
  },

  resolve: {
    modules: [path.resolve(__dirname, './app/Resources/assets'), 'node_modules']
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name     : 'vendor',
      filename : 'vendor.js',
      minChunks: 2,
    }),

    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    }),

    new webpack.LoaderOptionsPlugin({
      debug: true
    }),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: 'source-map',
      compress : {
        warnings: false
      }
    }),

    new webpack.DefinePlugin({
      app    : {
        __VERSION__: JSON.stringify(pkg.version),
        __TITLE__  : JSON.stringify(pkg.name),
      },
      process: {
        env: {
          NODE_ENV: JSON.stringify('development')
        },
      }
    }),
  ],
};
