const path    = require('path');
const webpack = require('webpack');

const webpackDevConfig = {
  overrides: {
    devtool: 'eval-source-map', // js moche - scss ok
    devServer: {
      contentBase: path.join(__dirname, 'web/'),
      historyApiFallback: true,
      stats: 'errors-only',
      host: 'localhost',
      port: 8090
    }
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    })
  ]
};

module.exports = require('./webpack.config')(webpackDevConfig);
