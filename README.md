symfony-webpack-221
===================

- In one console, run symfony backend server with :

 `bin/console server:run localhost:8000` (you can change port)

- In another console, run front server with :

`./node_modules/.bin/webpack-dev-server`

- Go to `localhost:8000` (change port accordingly to symfony server)

- Open Firefox/Chrome dev toolbar to see errors. 

