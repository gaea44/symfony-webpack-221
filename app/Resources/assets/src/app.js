// Jquery
import 'expose-loader?$!expose-loader?jQuery!jquery';

// Boostrap
import 'bootstrap-sass';

// Other vendor
import 'bootstrap-fileinput';
import 'bootstrap-fileinput/js/locales/fr';

$('[type=file]').fileinput();
